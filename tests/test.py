import unittest
from first_project import main


class TestGreet(unittest.TestCase):

    def test_greet_message(self):
        message = main.greet_user("Gitlab")
        self.assertEqual(message, "Hello Gitlab!")


if __name__ == '__main__':
    unittest.main()
