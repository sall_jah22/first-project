FROM python:3.11.2-slim-buster as builder

COPY . /first_project
WORKDIR /first_project

RUN python -m pip install -U pip && \
    python -m pip install poetry poetry-dynamic-versioning
RUN poetry build

FROM python:3.11.2-slim-buster as base

COPY --from=builder /first_project/dist/* /dist/
RUN pip --no-cache-dir install dist/*.whl

ENTRYPOINT ["first_project"]